/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fit5042.tutex.repository;

import fit5042.tutex.repository.entities.Property;
import java.util.ArrayList;
import java.util.List;

/**
 * TODO Exercise 1.3 Step 2 Complete this class.
 * 
 * This class implements the PropertyRepository class. You will need to add the keyword
 * "implements" PropertyRepository. 
 * 
 * @author qipeng
 */
public class SimplePropertyRepositoryImpl implements PropertyRepository{
	private List<Property> propertyRepositories;
	

    public SimplePropertyRepositoryImpl() {
    	this.propertyRepositories = new ArrayList<Property>();
        
    }

	@Override
	public void addProperty(Property property) throws Exception {
		
		if ((!propertyRepositories.contains(property)) && searchPropertyById(property.getId()) == null)
            this.propertyRepositories.add(property);
        else
            System.out.println("This property already exists.");
		
	}

	@Override
	public Property searchPropertyById(int id) throws Exception {
		
	        for (Property property : propertyRepositories) {
	            if (property.getId() == id)
	                return property;
	        }
	        return null;
	}

	@Override
	public List<Property> getAllProperties() throws Exception {
		List<Property> properties = new ArrayList<>(this.propertyRepositories.size());
        for (Property property : this.propertyRepositories) {
            properties.add(property);
        }
        return properties;
    }
		
	

	public List<Property> getPropertyRepositories() {
		return propertyRepositories;
	}

	public void setPropertyRepositories(ArrayList<Property> propertyRepositories) {
		this.propertyRepositories = propertyRepositories;
	}
    
    
}
